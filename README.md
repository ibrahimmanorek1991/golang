## Golang Using Framework Gin Gonic and glide

Under that folder you need to run below command to install required library dependency file glide.yaml

```
glide up
```

## How to compile and run the code ?

enter the directory project

```
go run main.go
```

## How do we know this application server is running?
You can try calling the root context of url
for the example, the base url of this server is

http://localhost:8383/unipin/

Just call it in normal browser and you will get message something like

```
{
    "message ": "Application running",
    "start_time ": "[27 September 2018] 10:32:43 WIB"
}
```

# How to test with Postman ?

When the server is up, you can use a Postman apps (chrome plugins) to do testing for every API provided. Import the postman collection API to your Postman from the `postman` folder.

Method GET
http://localhost:8383/unipin/

Author Ibrahim Manorek