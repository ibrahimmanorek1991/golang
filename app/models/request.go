package models

type Request struct {
	Partner_guid      string `json:"partner_guid"`
	Denomination_code string `json:"denomination_code"`
	Quantity          int    `json:"quantity"`
	Reference_no      int    `json:"reference_no"`
	Signature         string `jso:"signature"`
}
