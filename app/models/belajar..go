package models

type Belajar struct {
	ID      uint   `json:"id"`
	Message string `json:"message"`
	Data    []Objek
}
