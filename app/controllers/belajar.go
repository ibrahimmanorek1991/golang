package controllers

import (
	"fmt"
	"net/http"
	"unipin/app/models"

	"github.com/gin-gonic/gin"
)

func belajar(c *gin.Context) {

	fmt.Println("Tes")
	// res := map[string]string{
	// 	"start_time ": "data",
	// 	"message ":    "OK",
	// }

	data := []models.Objek{models.Objek{"ibrahim"}, models.Objek{"java programmer"}}

	res := models.Belajar{1, "coba", data}

	c.JSON(http.StatusOK, res)
}
