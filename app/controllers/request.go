package controllers

import (
	"fmt"
	"net/http"
	"strconv"
	"unipin/app/models"

	"github.com/gin-gonic/gin"
)

func RequestPost(c *gin.Context) {
	var jsonParam models.Request

	c.BindJSON(&jsonParam)
	fmt.Println(">>> ", jsonParam.Denomination_code)

	if jsonParam.Denomination_code == "" {
		c.JSON(http.StatusOK, models.Response{"denomination not found", "error"})
		return
	}
	ref := strconv.Itoa(jsonParam.Reference_no)
	res := models.Response{ref, "Success"}

	c.JSON(http.StatusOK, res)
}
