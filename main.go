package main

import (
	"fmt"
	"net/http"
	"unipin/app/controllers"
	"unipin/app/utils"

	"github.com/gin-gonic/gin"
)

var router *gin.Engine

func main() {
	fmt.Println("Starting..")

	router = gin.Default()
	port := "8383"
	Init()
	router.Run(":" + port)

}

func Init() {

	// router.GET("/test", Ping)
	// router.POST("/request", controllers.RequestPost)
	v1 := router.Group("/unipin")
	v1.GET("/", Ping)
	v1.POST("/request", controllers.RequestPost)
}

func Ping(c *gin.Context) {
	fmt.Println("Masuk")
	res := map[string]string{
		"start_time ": utils.GetCurrentTimeTimeZone("Asia/Jakarta"),
		"message ":    "Application running",
	}

	c.JSON(http.StatusOK, res)
}
